/**
 * Logger
 * Copyright (C) 2014 Szymon Kuchnia (szymon@kuchnia.name)
 *
 * Autor nie ponoszą odpowiedzialności za jakiekolwiek szkody wynikające z korzystania z danego oprogramowania.
 *
 * Udziela się zgody do korzystania z tego oprogramowania w dowolnym celu, w tym zastosowań
 * komercyjnych, a także zmieniać je i rozpowszechniać dowolnie z zastrzeżeniem następujących ograniczeń:
 *
 * 1. Pochodzenie oprogramiwania nie moze być nieprawdziwe.
 *    Państwo nie mogą twierdzić że napisali dane oprogramowanie.
 *
 * 2. Zmienione wersje źródłowe musza być oznaczone i nie mogą być porównywane
 *    z wersjami orginalnymi.
 *
 * 3. Ta informacja nie moze być zmieniona lub usunięta w każdej dystrybucji źródłowej.
 */

#include <Logger/Logger.h>

#include <iostream>

#include <cassert>

namespace Logger {

    /************************************************/
    Log::Log(const Type &type, const std::string &filename) {
        switch(type) {
            case Type::Consol:
                addOutput(new Logger::ConsolOutput);
              break;

            case Type::Txt:
                addOutput(new Logger::TextOutput(filename.empty() ? "logger.log" : filename));
              break;

            case Type::Html:
                addOutput(new Logger::HtmlOutput(filename.empty() ? "logger.html" : filename));
              break;

            default:
                assert(false); /* Typ nie został podięty */
              break;
        }
    }

}
