/**
 * Logger
 * Copyright (C) 2014 Szymon Kuchnia (szymon@kuchnia.name)
 *
 * Autor nie ponoszą odpowiedzialności za jakiekolwiek szkody wynikające z korzystania z danego oprogramowania.
 *
 * Udziela się zgody do korzystania z tego oprogramowania w dowolnym celu, w tym zastosowań
 * komercyjnych, a także zmieniać je i rozpowszechniać dowolnie z zastrzeżeniem następujących ograniczeń:
 *
 * 1. Pochodzenie oprogramiwania nie moze być nieprawdziwe.
 *    Państwo nie mogą twierdzić że napisali dane oprogramowanie.
 *
 * 2. Zmienione wersje źródłowe musza być oznaczone i nie mogą być porównywane
 *    z wersjami orginalnymi.
 *
 * 3. Ta informacja nie moze być zmieniona lub usunięta w każdej dystrybucji źródłowej.
 */

#include <Logger/Core/StreamLogger.h>
#include <Logger/Core/Output/AOutput.h>
#include <Logger/Core/Utils/Date.h>

#include <cstdlib>
#include <cstdarg>
#include <sstream>
#include <algorithm>

namespace Logger {

    /************************************************/
    StreamLogger::~StreamLogger() {
        for(auto it = outputs.begin(); it != outputs.end(); ++it) {
//            (*it)->write("Odpięto\n", LP_Debug);
            (*it)->deInit();
            delete *it;
        }

        outputs.clear();
    }


    /************************************************/
    bool StreamLogger::addOutput(AOutput *output) {
        if(std::find(outputs.begin(), outputs.end(), output) != outputs.end())
            return true; // Już istnieje

        output->init();
        outputs.push_back(output);
//        output->write("Podpięto\n", LP_Debug);

      return true;
    }


    /************************************************/
    bool StreamLogger::removeOutput(AOutput *output) {
        auto it = std::find(outputs.begin(), outputs.end(), output);

        if(it == outputs.end())
            return false; // Nie istnieje

//        output->write("Odpięto\n", LP_Debug);
        output->deInit();
        outputs.erase(it);

      return true;
    }


    /************************************************/
    void StreamLogger::write() {
        info.date = Date::GetDateTime();

        for(auto it = outputs.begin(); it != outputs.end(); ++it) {
            if(currentPriority == LP_Fatal) /** Błędy krytyczne nie podlegają filtracji!!! */
                (*it)->write(currentMsg.str(), currentPriority, &info);

            if((*it)->getPriorityFilter() & currentPriority)
                (*it)->write(currentMsg.str(), currentPriority, &info);
        }

        currentMsg.clear();
        currentMsg.str("");

        /* Czyścimy dodatkowe informajcę o logu */
        info.date     = "";
        info.file     = "";
        info.function = "";
        info.line     = -1;
    }


    /************************************************/
    void StreamLogger::log(const Priority &priority, const char *format, ...) {
        if(format == NULL)
            return;

        currentPriority = priority;

        va_list argList;
        va_start(argList, format);

         char tempText[1024];
        if(vsnprintf(tempText,sizeof(tempText),format, argList) == -1)
            return;

        if(!currentMsg.str().empty())
            write(); //wypisz wszystko na wyjście

        currentMsg.str(std::string(tempText) + "\r\n");
        write();
    }

}
