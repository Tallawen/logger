/**
 * Logger
 * Copyright (C) 2014 Szymon Kuchnia (szymon@kuchnia.name)
 *
 * Autor nie ponoszą odpowiedzialności za jakiekolwiek szkody wynikające z korzystania z danego oprogramowania.
 *
 * Udziela się zgody do korzystania z tego oprogramowania w dowolnym celu, w tym zastosowań
 * komercyjnych, a także zmieniać je i rozpowszechniać dowolnie z zastrzeżeniem następujących ograniczeń:
 *
 * 1. Pochodzenie oprogramiwania nie moze być nieprawdziwe.
 *    Państwo nie mogą twierdzić że napisali dane oprogramowanie.
 *
 * 2. Zmienione wersje źródłowe musza być oznaczone i nie mogą być porównywane
 *    z wersjami orginalnymi.
 *
 * 3. Ta informacja nie moze być zmieniona lub usunięta w każdej dystrybucji źródłowej.
 */

#include <Logger/Core/Priority.h>

#include <map>
#include <cassert>

namespace Logger {

    /************************************************
     * Nazwy poszczególnych priorytetów
     ************************************************/
    static std::map<Priority, std::string> MapPriorityToStr = {
            {LP_Fatal,   "Fatal"},
            {LP_Error,   "Error"},
            {LP_Success, "Success"},
            {LP_Info,    "Info"},
            {LP_Warning, "Warning"},
            {LP_Debug,   "Debug"}
    };


    /************************************************/
    std::string PriorityToStr(const Priority &priority) {
        if(MapPriorityToStr.find(priority) == MapPriorityToStr.end())
            assert(false); // Dodaj priorytet do mapy (MapPriorityToStr)

      return MapPriorityToStr[priority];
    }

}
