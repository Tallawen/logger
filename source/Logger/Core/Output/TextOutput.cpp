/**
 * Logger
 * Copyright (C) 2014 Szymon Kuchnia (szymon@kuchnia.name)
 *
 * Autor nie ponoszą odpowiedzialności za jakiekolwiek szkody wynikające z korzystania z danego oprogramowania.
 *
 * Udziela się zgody do korzystania z tego oprogramowania w dowolnym celu, w tym zastosowań
 * komercyjnych, a także zmieniać je i rozpowszechniać dowolnie z zastrzeżeniem następujących ograniczeń:
 *
 * 1. Pochodzenie oprogramiwania nie moze być nieprawdziwe.
 *    Państwo nie mogą twierdzić że napisali dane oprogramowanie.
 *
 * 2. Zmienione wersje źródłowe musza być oznaczone i nie mogą być porównywane
 *    z wersjami orginalnymi.
 *
 * 3. Ta informacja nie moze być zmieniona lub usunięta w każdej dystrybucji źródłowej.
 */

#include <Logger/Core/Output/TextOutput.h>
#include <Logger/Core/Utils/Date.h>

#include <sstream>
#include <string>
#include <iostream>

namespace Logger {

    /************************************************/
    bool TextOutput::init() {
        stream.open(filename.c_str(), std::ios_base::out | std::ios_base::trunc);

        if(!stream.is_open())
            return false;

        stream << std::endl
               << "--- Log utworzony (" << Date::GetDateTime() << ')' << std::endl // data w formacie rrrr-mm-dd : hh:mm:ss
               << std::endl << std::endl;

      return true;
    }


    /************************************************/
    bool TextOutput::deInit() {
        stream << std::endl
               << "--- Log zamknięty (" << Date::GetDateTime() << ')' << std::endl // data w formacie rrrr-mm-dd : hh:mm:ss
               << std::endl << std::endl;

        stream.close();

      return stream.is_open();
    }


    /************************************************/
    void TextOutput::write(const std::string &msg, const Priority &priority, LogInfo *logInfo) {
        if(!stream.is_open())
            return;

        stream << PriorityToStr(priority);

        if(logInfo != nullptr) {
            stream << "| " << logInfo->date;

            if(!logInfo->file.empty())
                stream << "| " << logInfo->file;
            if(logInfo->line >= 0)
                stream << "| " << logInfo->line;
            if(!logInfo->function.empty())
                stream << "| " << logInfo->function;
        }

        stream << ": " << msg << std::endl;
    }

}

