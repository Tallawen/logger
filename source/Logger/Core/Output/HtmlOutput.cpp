/**
 * Logger
 * Copyright (C) 2014 Szymon Kuchnia (szymon@kuchnia.name)
 *
 * Autor nie ponoszą odpowiedzialności za jakiekolwiek szkody wynikające z korzystania z danego oprogramowania.
 *
 * Udziela się zgody do korzystania z tego oprogramowania w dowolnym celu, w tym zastosowań
 * komercyjnych, a także zmieniać je i rozpowszechniać dowolnie z zastrzeżeniem następujących ograniczeń:
 *
 * 1. Pochodzenie oprogramiwania nie moze być nieprawdziwe.
 *    Państwo nie mogą twierdzić że napisali dane oprogramowanie.
 *
 * 2. Zmienione wersje źródłowe musza być oznaczone i nie mogą być porównywane
 *    z wersjami orginalnymi.
 *
 * 3. Ta informacja nie moze być zmieniona lub usunięta w każdej dystrybucji źródłowej.
 */

#include <Logger/Core/Output/HtmlOutput.h>
#include <Logger/Core/Utils/Date.h>

#include <sstream>
#include <string>
#include <algorithm>
#include <iostream>
#include <ctype.h>

namespace Logger {

    /************************************************/
    std::string toString(const int &number) {
        std::stringstream ss;
        ss << number;

      return ss.str();
    }


    /************************************************/
    bool HtmlOutput::init() {
        stream.open(filename.c_str(), std::ios_base::out | std::ios_base::trunc);

        if(!stream.is_open())
            return false;

        stream << "<!DOCTYPE html>\n"
                  "<html lang=\"pl\">\n"
                  "    <head>\n"
                  "        <meta charset=\"utf-8\" />\n"
                  "        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\" />\n"
                  "        <meta name=\"viewport\" content=\"width=device-width; initial-scale=1.0\" />\n"
                  "        <link rel=\"stylesheet\" href=\"style.css\" type=\"text/css\"/>\n"
                  "        <script src=\"script.js\" type=\"text/javascript\"></script>\n"
                  "        <title>Logger</title>\n"
                  "    </head>\n"
                  "    <body>\n"
                  "        <div id=\"top\"> </div>\n"
                  "        <div id=\"options\">\n"
                  "            <form>\n"
                  "                <fieldset>\n"
                  "                    Display:\n"
                  "                    <label> <input type=\"checkbox\" id=\"none\"    checked onchange=\"toggle('hide-none',    this)\" /> None </label>\n"
                  "                    <label> <input type=\"checkbox\" id=\"info\"    checked onchange=\"toggle('hide-info',    this)\" /> Info </label>\n"
                  "                    <label> <input type=\"checkbox\" id=\"success\" checked onchange=\"toggle('hide-success', this)\" /> Success </label>\n"
                  "                    <label> <input type=\"checkbox\" id=\"warning\" checked onchange=\"toggle('hide-warning', this)\" /> Warning </label>\n"
                  "                    <label> <input type=\"checkbox\" id=\"error\"   checked onchange=\"toggle('hide-error',   this)\" /> Error </label>\n"
                  "                    <label> <input type=\"checkbox\" id=\"fatal\"   checked onchange=\"toggle('hide-fatal',   this)\" /> Fatal </label>\n"
                  "                    <label> <input type=\"checkbox\" id=\"debug\"   checked onchange=\"toggle('hide-debug',   this)\" /> Debug </label>\n"
                  "                </fieldset>\n"
                  "                <fieldset>\n"
                  "                    Sort by:\n"
                  "                    <button type=\"button\" onclick=\"sort(0)\"> Status </button>\n"
                  "                    <button type=\"button\" onclick=\"sort(1)\"> Date </button>\n"
//                  "                    <button type=\"button\" onclick=\"sort(2)\"> Path </button>\n"
//                  "                    <button type=\"button\" onclick=\"sort(3)\"> Function </button>\n"
//                  "                    <button type=\"button\" onclick=\"sort(4)\"> Line </button>\n"
//                  "                    <button type=\"button\" onclick=\"sort(5)\"> Message </button>\n"
                  "                </fieldset>\n"
                  "            </form>\n"
                  "        </div>\n"
                  "        <table id=\"log\">\n"
                  "            <tbody>\n";

      return true;
    }


    /************************************************/
    bool HtmlOutput::deInit() {
        stream << "            </tbody>\n"
                  "        </table>\n"
                  "    </body>\n"
                  "</html>";

        stream.close();

      return stream.is_open();
    }


    /************************************************/
    void HtmlOutput::write(const std::string &msg, const Priority &priority, LogInfo *logInfo) {
        if(!stream.is_open())
            return;

        std::string priorityStr = PriorityToStr(priority);
        std::transform(priorityStr.begin(), priorityStr.end(), priorityStr.begin(), ::tolower);

        stream << "                <tr class=\"" << priorityStr << "\"> <td>" << int(priority) << "</td>";

        if(logInfo != nullptr) {
            stream << "<td> " << logInfo->date << " </td>";

            stream << "<td> " << (!logInfo->file.empty()     ? logInfo->file           : "---") << " </td>";
            stream << "<td> " << (!logInfo->function.empty() ? logInfo->function       : "---") << " </td>";
            stream << "<td> " << (logInfo->line >= 0         ? toString(logInfo->line) : "---") << " </td>";

        } else {
            stream << "<td> --- </td>"; // data;
            stream << "<td> --- </td>"; // plik;
            stream << "<td> --- </td>"; // funkcja;
            stream << "<td> --- </td>"; // linia;
        }

         std::string str = msg;
        for(auto pos = str.find("\n"); pos != std::string::npos; pos = str.find("\n")) { /* Wstawiamy <br/> w miejsce znaku nowej linii */
            str.erase(pos, 1);
            str.insert(pos, "<br/>");
        }

        stream << "<td> " << str << " </td> </tr>" << std::endl;
    }

}

