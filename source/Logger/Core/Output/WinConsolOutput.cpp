/**
 * Logger
 * Copyright (C) 2014 Szymon Kuchnia (szymon@kuchnia.name)
 *
 * Autor nie ponoszą odpowiedzialności za jakiekolwiek szkody wynikające z korzystania z danego oprogramowania.
 *
 * Udziela się zgody do korzystania z tego oprogramowania w dowolnym celu, w tym zastosowań
 * komercyjnych, a także zmieniać je i rozpowszechniać dowolnie z zastrzeżeniem następujących ograniczeń:
 *
 * 1. Pochodzenie oprogramiwania nie moze być nieprawdziwe.
 *    Państwo nie mogą twierdzić że napisali dane oprogramowanie.
 *
 * 2. Zmienione wersje źródłowe musza być oznaczone i nie mogą być porównywane
 *    z wersjami orginalnymi.
 *
 * 3. Ta informacja nie moze być zmieniona lub usunięta w każdej dystrybucji źródłowej.
 */

#include <Logger/Core/Output/WinConsolOutput.h>
#include <Logger/Core/Priority.h>
#include <Logger/Core/Utils/Date.h>

#include <map>
#include <cstdlib>
#include <sstream>
#include <iostream>
#include <algorithm>

#ifdef LOGGER_SYSTEM_WINDOWS

    namespace Logger {

        /************************************************
         * Kolory dla poszczególnych Priorytetów
         ************************************************/
        static std::map<Priority, WORD> PriorityColor = {
            {LP_Info,    FOREGROUND_RED   | FOREGROUND_GREEN     | FOREGROUND_BLUE | FOREGROUND_INTENSITY}, // Biały
            {LP_Success, FOREGROUND_GREEN | FOREGROUND_INTENSITY}, // Zielony
            {LP_Warning, FOREGROUND_RED   | FOREGROUND_GREEN     | FOREGROUND_INTENSITY}, // Żółty
            {LP_Error,   FOREGROUND_RED   | FOREGROUND_INTENSITY}, // Czerwony
            {LP_Fatal,   FOREGROUND_RED   | FOREGROUND_INTENSITY | BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_INTENSITY}, // Biały na czerwonym tle
            {LP_Debug,   FOREGROUND_BLUE  | FOREGROUND_INTENSITY}, // Niebieski
            {LP_None,    BACKGROUND_RED   | BACKGROUND_GREEN     | BACKGROUND_BLUE | BACKGROUND_INTENSITY | FOREGROUND_INTENSITY} // Czarny na białym tle
        };


        /************************************************/
        bool WinConsolOutput::init() {
            AllocConsole();
            handleOut = GetStdHandle(STD_OUTPUT_HANDLE);

            if(handleOut == nullptr) // uchwyt nie został ustawiony
                return false;

             CONSOLE_SCREEN_BUFFER_INFO screenInfo;
            GetConsoleScreenBufferInfo(handleOut, &screenInfo);

            originalAttributes = screenInfo.wAttributes;

             std::stringstream ss;
            ss << std::endl
               << "--- Log utworzony (" << Date::GetDateTime() << ')' << std::endl // data w formacie rrrr-mm-dd : hh:mm:ss
               << std::endl << std::endl;

            write(ss.str(), LP_Info);
          return true;
        }


        /************************************************/
        bool WinConsolOutput::deInit() {
            std::stringstream ss;
           ss << std::endl
              << "--- Log zamknięty (" << Date::GetDateTime() << ')' << std::endl // data w formacie rrrr-mm-dd : hh:mm:ss
              << std::endl << std::endl;

           write(ss.str(), LP_Info);
         return true;
        }


        /************************************************/
        void WinConsolOutput::write(const std::string &msg, const Priority &priority, LogInfo *logInfo) {
            /* Gdy dla danego priorytetu nie istnieje kolor lub dany priorytet nie występuje w bazie zostaje ustawiony kolor odnoszący się do (LP_None) */
            DWORD color = (PriorityColor.find(priority) != PriorityColor.end()) ? PriorityColor[priority] : PriorityColor[LP_None];

            SetConsoleCP(65001);
            SetConsoleTextAttribute(handleOut, color);

            DWORD dwTemp;

             std::string  priorityStr = PriorityToStr(priority);
             std::wstring priorityWStr(priorityStr.begin(), priorityStr.end());
            WriteConsoleW(handleOut, priorityWStr.c_str(), priorityWStr.size(), &dwTemp, NULL);

            if(logInfo != nullptr) {
                if(!logInfo->file.empty()) {
                     std::wstring str(logInfo->file.begin(), logInfo->file.end());
                     str = L"| " + str;
                    WriteConsoleW(handleOut, str.c_str(), str.size(), &dwTemp, NULL);
                }

                if(!logInfo->function.empty()) {
                    std::wstring str(logInfo->function.begin(), logInfo->function.end());
                    str = L"| " + str;
                   WriteConsoleW(handleOut, str.c_str(), str.size(), &dwTemp, NULL);
                }
            }

             std::wstring str(msg.begin(), msg.end());
             str = L": " + str + L"\n";
            WriteConsoleW(handleOut, str.c_str(), str.size(), &dwTemp, NULL);

            SetConsoleTextAttribute(handleOut, originalAttributes);
        }

    }

#endif /* LOGGER_SYSTEM_WINDOWS */
