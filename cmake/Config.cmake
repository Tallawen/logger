# detect the OS
if(${CMAKE_SYSTEM_NAME} MATCHES "Windows")
    set(LOGGER_OS_WINDOWS 1)

    # detect the architecture (note: this test won't work for cross-compilation)
    include(CheckTypeSize)
    check_type_size(void* SIZEOF_VOID_PTR)
    if("${SIZEOF_VOID_PTR}" STREQUAL "4")
        set(ARCH_32BITS 1)
    elseif("${SIZEOF_VOID_PTR}" STREQUAL "8")
        set(ARCH_64BITS 1)
    else()
        message(FATAL_ERROR "Unsupported architecture")
        return()
    endif()
elseif(${CMAKE_SYSTEM_NAME} MATCHES "Linux")
    set(LOGGER_OS_LINUX 1)

else()
    message(FATAL_ERROR "Unsupported operating system")
    return()
	
endif()

# detect the compiler and its version
# Note: on some platforms (OS X), CMAKE_COMPILER_IS_GNUCXX is true
# even when CLANG is used, therefore the Clang test is done first
if(CMAKE_CXX_COMPILER MATCHES ".*clang[+][+]" OR CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
   # CMAKE_CXX_COMPILER_ID is an internal CMake variable subject to change,
   # but there is no other way to detect CLang at the moment
   set(LOGGER_COMPILER_CLANG 1)
   execute_process(COMMAND "${CMAKE_CXX_COMPILER}" "--version" OUTPUT_VARIABLE CLANG_VERSION_OUTPUT)
   string(REGEX REPLACE ".*clang version ([0-9]+\\.[0-9]+).*" "\\1" LOGGER_CLANG_VERSION "${CLANG_VERSION_OUTPUT}")
elseif(CMAKE_COMPILER_IS_GNUCXX)
    set(LOGGER_COMPILER_GCC 1)
    execute_process(COMMAND "${CMAKE_CXX_COMPILER}" "-dumpversion" OUTPUT_VARIABLE GCC_VERSION_OUTPUT)
    string(REGEX REPLACE "([0-9]+\\.[0-9]+).*" "\\1" SLOGGER_GCC_VERSION "${GCC_VERSION_OUTPUT}")
    execute_process(COMMAND "${CMAKE_CXX_COMPILER}" "--version" OUTPUT_VARIABLE GCC_COMPILER_VERSION)
    string(REGEX MATCHALL ".*(tdm[64]*-[1-9]).*" LOGGER_COMPILER_GCC_TDM "${GCC_COMPILER_VERSION}")
    execute_process(COMMAND "${CMAKE_CXX_COMPILER}" "-dumpmachine" OUTPUT_VARIABLE GCC_MACHINE)
    string(STRIP "${GCC_MACHINE}" GCC_MACHINE)
    if(${GCC_MACHINE} MATCHES ".*w64.*")
        set(LOGGER_COMPILER_GCC_W64 1)
    endif()
elseif(MSVC)
    set(LOGGER_COMPILER_MSVC 1)
    if(MSVC_VERSION EQUAL 1400)
        set(LOGGER_MSVC_VERSION 8)
    elseif(MSVC_VERSION EQUAL 1500)
        set(LOGGER_MSVC_VERSION 9)
    elseif(MSVC_VERSION EQUAL 1600)
        set(LOGGER_MSVC_VERSION 10)
    elseif(MSVC_VERSION EQUAL 1700)
        set(LOGGER_MSVC_VERSION 11)
    elseif(MSVC_VERSION EQUAL 1800)
        set(LOGGER_MSVC_VERSION 12)
    endif()
else()
    message(FATAL_ERROR "Unsupported compiler")
    return()
endif()

ADD_DEFINITIONS(
    -std=c++11 # Or -std=c++0x
    # Other flags
)

# define the install directory for miscellaneous files
if(LOGGER_OS_WINDOWS)
    set(INSTALL_MISC_DIR .)
elseif(LOGGER_OS_LINUX)
    set(INSTALL_MISC_DIR share/LOGGER)
endif()