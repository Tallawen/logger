/**
 * Logger
 * Copyright (C) 2014 Szymon Kuchnia (szymon@kuchnia.name)
 *
 * Autor nie ponoszą odpowiedzialności za jakiekolwiek szkody wynikające z korzystania z danego oprogramowania.
 *
 * Udziela się zgody do korzystania z tego oprogramowania w dowolnym celu, w tym zastosowań
 * komercyjnych, a także zmieniać je i rozpowszechniać dowolnie z zastrzeżeniem następujących ograniczeń:
 *
 * 1. Pochodzenie oprogramiwania nie moze być nieprawdziwe.
 *    Państwo nie mogą twierdzić że napisali dane oprogramowanie.
 *
 * 2. Zmienione wersje źródłowe musza być oznaczone i nie mogą być porównywane
 *    z wersjami orginalnymi.
 *
 * 3. Ta informacja nie moze być zmieniona lub usunięta w każdej dystrybucji źródłowej.
 */

#ifndef __LOGGER_H__
#define __LOGGER_H__

#include <Logger/Core/Priority.h>

#include <Logger/Core/Output/TextOutput.h>
#include <Logger/Core/Output/ConsolOutput.h>
#include <Logger/Core/Output/HtmlOutput.h>

#include <Logger/Core/Manipulator.h>

#include <Logger/Core/StreamLogger.h>

#include <Logger/Core/Utils/Date.h>

namespace Logger {

    #ifndef LOG_INFO
        #define LOG_INFO(logger) logger.info.file = __FILE__; \
                                 logger.info.function = __FUNCTION__; \
                                 logger.info.line = __LINE__; \
                                 logger
    #endif /*LOG_INFO*/

    class LOGGER_API Log : public StreamLogger {
    public:
        enum class Type {
            Consol,
            Txt,
            Html
        };

    public:
        Log(const Type &type = Type::Txt, const std::string &filename = "logger.log");
        ~Log() {}
    };

}

#endif /* __LOGGER_H__ */
