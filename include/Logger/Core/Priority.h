/**
 * Logger
 * Copyright (C) 2014 Szymon Kuchnia (szymon@kuchnia.name)
 *
 * Autor nie ponoszą odpowiedzialności za jakiekolwiek szkody wynikające z korzystania z danego oprogramowania.
 *
 * Udziela się zgody do korzystania z tego oprogramowania w dowolnym celu, w tym zastosowań
 * komercyjnych, a także zmieniać je i rozpowszechniać dowolnie z zastrzeżeniem następujących ograniczeń:
 *
 * 1. Pochodzenie oprogramiwania nie moze być nieprawdziwe.
 *    Państwo nie mogą twierdzić że napisali dane oprogramowanie.
 *
 * 2. Zmienione wersje źródłowe musza być oznaczone i nie mogą być porównywane
 *    z wersjami orginalnymi.
 *
 * 3. Ta informacja nie moze być zmieniona lub usunięta w każdej dystrybucji źródłowej.
 */

#ifndef __LOGGER_PRIORITY_H__
#define __LOGGER_PRIORITY_H__

#include <string>

namespace Logger {

    /**********************************************//**
     * @brief Priorytety wiadomości
     ************************************************/
    enum Priority {
        LP_None    = 0x00, /**< Flaga dla nieprzyporządkowanego priorytetu*/
        LP_Fatal   = 0x01, /**< Bład krytyczny - nigdy nie filtrowany */
        LP_Info    = 0x02,
        LP_Success = 0x04,
        LP_Warning = 0x08,
        LP_Error   = 0x10,
        LP_Debug   = 0x40
    };

    const unsigned int LP_Filter_Default = (LP_Info | LP_Success | LP_Warning | LP_Error | LP_Debug); /**< Domyślna wartość filtru */

    /**********************************************//**
     * @brief Przekształca elementy typu wyliczeniowego @b Priority na odpowiadające im nazwy
     ************************************************/
    std::string PriorityToStr(const Priority &priority);
}


#endif /*__LOGGER_PRIORITY_H__*/
