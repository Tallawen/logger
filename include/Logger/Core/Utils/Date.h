/**
 * Logger
 * Copyright (C) 2014 Szymon Kuchnia (szymon@kuchnia.name)
 *
 * Autor nie ponoszą odpowiedzialności za jakiekolwiek szkody wynikające z korzystania z danego oprogramowania.
 *
 * Udziela się zgody do korzystania z tego oprogramowania w dowolnym celu, w tym zastosowań
 * komercyjnych, a także zmieniać je i rozpowszechniać dowolnie z zastrzeżeniem następujących ograniczeń:
 *
 * 1. Pochodzenie oprogramiwania nie moze być nieprawdziwe.
 *    Państwo nie mogą twierdzić że napisali dane oprogramowanie.
 *
 * 2. Zmienione wersje źródłowe musza być oznaczone i nie mogą być porównywane
 *    z wersjami orginalnymi.
 *
 * 3. Ta informacja nie moze być zmieniona lub usunięta w każdej dystrybucji źródłowej.
 */

#ifndef __LOGGER_UTILS_DATE_H__
#define __LOGGER_UTILS_DATE_H__

#include <iostream>
#include <string>
#include <ctime>
#include <sstream>

namespace Logger {

    class Date {
    public:
        tm * currentTime;
        std::stringstream result;

    protected: /* Obiektu nie można stworzyć poza klasą */
        Date();
        //~Date();

    public:
        /**********************************************//**
          Funkcja zwracająca date w podanym formacie
          np: Format("%D.%M.%Y r")
          zwroci : dzien:miesiac:rok r
          nierozpoznane argumenty sa przepisywane
          wszystkie argumenty opisane w ciele metody
         ************************************************/
        static std::string Format(const std::string &format);

        /**********************************************//**
          Data w formacie yyyy-mm-dd
         ************************************************/
        static std::string GetDate();

        /**********************************************//**
          Godzina w formacie hh:mm:ss
         ************************************************/
        static std::string GetTime();

        /**********************************************//**
          Data i godzina w formacie yyyy-mm-dd : hh:mm:ss
         ************************************************/
        static std::string GetDateTime();

        /*********************************************
          Pobiera aktualny czas z systemu
         ********************************************/
        inline void refresh();

        /********************************************/
        inline void day();

        /********************************************/
        inline void month();

        /********************************************/
        inline void year();

        /********************************************/
        inline void hour24();

        /********************************************/
        inline void hour12();

        /********************************************/
        inline void min();

        /********************************************/
        inline void sec();

        /********************************************/
        inline void am_pm();
    };
}

#endif /*__LOGGER_UTILS_DATE_H__*/
