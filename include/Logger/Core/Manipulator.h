/**
 * Logger
 * Copyright (C) 2014 Szymon Kuchnia (szymon@kuchnia.name)
 *
 * Autor nie ponoszą odpowiedzialności za jakiekolwiek szkody wynikające z korzystania z danego oprogramowania.
 *
 * Udziela się zgody do korzystania z tego oprogramowania w dowolnym celu, w tym zastosowań
 * komercyjnych, a także zmieniać je i rozpowszechniać dowolnie z zastrzeżeniem następujących ograniczeń:
 *
 * 1. Pochodzenie oprogramiwania nie moze być nieprawdziwe.
 *    Państwo nie mogą twierdzić że napisali dane oprogramowanie.
 *
 * 2. Zmienione wersje źródłowe musza być oznaczone i nie mogą być porównywane
 *    z wersjami orginalnymi.
 *
 * 3. Ta informacja nie moze być zmieniona lub usunięta w każdej dystrybucji źródłowej.
 */

#ifndef __LOGGER_MANIPULATOR_H__
#define __LOGGER_MANIPULATOR_H__

#include <Logger/Core/Export.h>
#include <Logger/Core/Priority.h>

namespace Logger {

    /**********************************************//**
     * @brief Manipulator do strumenia logger'a, który zmienia aktualny priorytet
     ************************************************/
    class LOGGER_API Prio {
    public:
        Priority priority;

    public:
        Prio(const Priority &priority) : priority(priority) {}
    };


    /**********************************************//**
     * @brief Manipulator wymusza wyproznienie bufora z wiadomoscia i wysłanie jej do wszystkich wyjść
     ************************************************/
    class Flush { };

}

#endif /* __LOGGER_MANIPULATOR_H__ */
