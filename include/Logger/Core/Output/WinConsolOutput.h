/**
 * Logger
 * Copyright (C) 2014 Szymon Kuchnia (szymon@kuchnia.name)
 *
 * Autor nie ponoszą odpowiedzialności za jakiekolwiek szkody wynikające z korzystania z danego oprogramowania.
 *
 * Udziela się zgody do korzystania z tego oprogramowania w dowolnym celu, w tym zastosowań
 * komercyjnych, a także zmieniać je i rozpowszechniać dowolnie z zastrzeżeniem następujących ograniczeń:
 *
 * 1. Pochodzenie oprogramiwania nie moze być nieprawdziwe.
 *    Państwo nie mogą twierdzić że napisali dane oprogramowanie.
 *
 * 2. Zmienione wersje źródłowe musza być oznaczone i nie mogą być porównywane
 *    z wersjami orginalnymi.
 *
 * 3. Ta informacja nie moze być zmieniona lub usunięta w każdej dystrybucji źródłowej.
 */

#ifndef __LOGGER_OUTPUT_WINCONSOLOUTPUT_H__
#define __LOGGER_OUTPUT_WINCONSOLOUTPUT_H__

#include <Logger/Core/Config.h>

#ifdef LOGGER_SYSTEM_WINDOWS

    #include <Logger/Core/Output/AOutput.h>

    #include <string>
    #include <fstream>
    #include <windows.h>
    #include <iostream>

    namespace Logger {

        /**********************************************//**
         * @brief Wyjście loggera na konsole Windows NT / MS-DOS.
         ************************************************/
        class LOGGER_API WinConsolOutput : public AOutput {
        public:

        private:
            HANDLE handleOut; /**< Uchwyt kosoli */
            WORD   originalAttributes; /**< Orginalne atrybuty konsoli */

        public:
            WinConsolOutput() : AOutput(), handleOut(nullptr), originalAttributes(0) { }
            virtual ~WinConsolOutput() {}

        public:
            /**********************************************//**
             * @copybrief AOutput::init()
             ************************************************/
            bool init();

            /**********************************************//**
             * @copybrief AOutput::deInit()
             ************************************************/
            bool deInit();

            /**********************************************//**
             * @copybrief AOutput::write(const std::string &msg)
             ************************************************/
            void write(const std::string &msg, const Priority &priority, LogInfo *logInfo = nullptr);
        };

    }

#endif /* LOGGER_SYSTEM_WINDOWS */

#endif /* __LOGGER_OUTPUT_WINCONSOLOUTPUT_H__ */
