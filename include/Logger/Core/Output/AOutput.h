/**
 * Logger
 * Copyright (C) 2014 Szymon Kuchnia (szymon@kuchnia.name)
 *
 * Autor nie ponoszą odpowiedzialności za jakiekolwiek szkody wynikające z korzystania z danego oprogramowania.
 *
 * Udziela się zgody do korzystania z tego oprogramowania w dowolnym celu, w tym zastosowań
 * komercyjnych, a także zmieniać je i rozpowszechniać dowolnie z zastrzeżeniem następujących ograniczeń:
 *
 * 1. Pochodzenie oprogramiwania nie moze być nieprawdziwe.
 *    Państwo nie mogą twierdzić że napisali dane oprogramowanie.
 *
 * 2. Zmienione wersje źródłowe musza być oznaczone i nie mogą być porównywane
 *    z wersjami orginalnymi.
 *
 * 3. Ta informacja nie moze być zmieniona lub usunięta w każdej dystrybucji źródłowej.
 */

#ifndef __LOGGER_OUTPUT_AOUTPUT_H__
#define __LOGGER_OUTPUT_AOUTPUT_H__

#include <Logger/Core/Export.h>
#include <Logger/Core/Priority.h>
#include <Logger/Core/LogInfo.h>

#include <string>

namespace Logger {

    /**********************************************//**
     * @brief Podstawowa klasa @b abstrakcyjna dla wszystkich wyjść logger'a
     ************************************************/
    class LOGGER_API AOutput {
    public:

    private:
        unsigned int priorityFilter;

    public:
        AOutput() : priorityFilter(LP_Filter_Default) {}
        virtual ~AOutput() { }

    public:
        /**********************************************//**
         * @brief Metoda inicjalizująca
         *
         * @retval true  Inicjalizajca powiodła się
         * @retval false Wystąpił błąd w trakcie inicjalizacji
         ************************************************/
        virtual bool init()   = 0;

        /**********************************************//**
         * @brief Metoda deinicjalizująca
         *
         * @retval true  Deinicjalizująca powiodła się
         * @retval false Wystąpił błąd w trakcie deinicjalizująca
         ************************************************/
        virtual bool deInit() = 0;

        /**********************************************//**
         * @brief Metoda zapisująca wiadomość
         *
         * @param msg      Wiadomość do zapisania
         * @param priority Priorytet danej wiadomości
         * @param logInfo  Dodatkowe informacje o logu
         ************************************************/
        virtual void write(const std::string &msg, const Priority &priority, LogInfo *logInfo = nullptr) = 0;

        /**********************************************//**
         * @brief Ustawia priorytet filtrowania
         ************************************************/
        void setPriorityFilter(const unsigned int &priority) {
            priorityFilter = priority;
        }

        /**********************************************//**
         * @brief Zwraca priorytet filtrowania
         ************************************************/
        unsigned int getPriorityFilter() const {
          return priorityFilter;
        }
    };
}

#endif /*__LOGGER_OUTPUT_AOUTPUT_H__*/
