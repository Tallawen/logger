/**
 * Logger
 * Copyright (C) 2014 Szymon Kuchnia (szymon@kuchnia.name)
 *
 * Autor nie ponoszą odpowiedzialności za jakiekolwiek szkody wynikające z korzystania z danego oprogramowania.
 *
 * Udziela się zgody do korzystania z tego oprogramowania w dowolnym celu, w tym zastosowań
 * komercyjnych, a także zmieniać je i rozpowszechniać dowolnie z zastrzeżeniem następujących ograniczeń:
 *
 * 1. Pochodzenie oprogramiwania nie moze być nieprawdziwe.
 *    Państwo nie mogą twierdzić że napisali dane oprogramowanie.
 *
 * 2. Zmienione wersje źródłowe musza być oznaczone i nie mogą być porównywane
 *    z wersjami orginalnymi.
 *
 * 3. Ta informacja nie moze być zmieniona lub usunięta w każdej dystrybucji źródłowej.
 */

#ifndef __LOGGER_OUTPUT_CONSOLOUTPUT_H__
#define __LOGGER_OUTPUT_CONSOLOUTPUT_H__

#include <Logger/Core/Config.h>

#if defined(LOGGER_SYSTEM_WINDOWS)
    #include <Logger/Core/Output/WinConsolOutput.h>

    namespace Logger {
        class LOGGER_API ConsolOutput : public WinConsolOutput {

        };
    }

#elif defined(LOGGER_SYSTEM_LINUX)


#endif

#endif /* __LOGGER_OUTPUT_CONSOLOUTPUT_H__ */
