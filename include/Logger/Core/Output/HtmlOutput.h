/**
 * Logger
 * Copyright (C) 2014 Szymon Kuchnia (szymon@kuchnia.name)
 *
 * Autor nie ponoszą odpowiedzialności za jakiekolwiek szkody wynikające z korzystania z danego oprogramowania.
 *
 * Udziela się zgody do korzystania z tego oprogramowania w dowolnym celu, w tym zastosowań
 * komercyjnych, a także zmieniać je i rozpowszechniać dowolnie z zastrzeżeniem następujących ograniczeń:
 *
 * 1. Pochodzenie oprogramiwania nie moze być nieprawdziwe.
 *    Państwo nie mogą twierdzić że napisali dane oprogramowanie.
 *
 * 2. Zmienione wersje źródłowe musza być oznaczone i nie mogą być porównywane
 *    z wersjami orginalnymi.
 *
 * 3. Ta informacja nie moze być zmieniona lub usunięta w każdej dystrybucji źródłowej.
 */

#ifndef __LOGGER_OUTPUT_HTMLOUTPUT_H__
#define __LOGGER_OUTPUT_HTMLOUTPUT_H__

#include <Logger/Core/Output/AOutput.h>

#include <string>
#include <fstream>

namespace Logger {

    /**********************************************//**
     * @brief Wyjście tekstowe logger'a
     ************************************************/
    class LOGGER_API HtmlOutput : public AOutput {
    public:

    private:
        std::fstream stream;  /**< Strumień pliku */
        std::string filename; /**< Nazwa pliku */

    public:
        HtmlOutput(const std::string &filename = "logger.html") : AOutput(), filename(filename) { }

        ~HtmlOutput() {}

    public:
        /**********************************************//**
         * @copybrief AOutput::init()
         ************************************************/
        bool init();

        /**********************************************//**
         * @copybrief AOutput::deInit()
         ************************************************/
        bool deInit();

        /**********************************************//**
         * @copybrief AOutput::write(const std::string &msg)
         ************************************************/
        void write(const std::string &msg, const Priority &priority, LogInfo *logInfo = nullptr);
    };

}

#endif /* __LOGGER_OUTPUT_HTMLOUTPUT_H__ */
