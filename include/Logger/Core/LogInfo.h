/**
 * Logger
 * Copyright (C) 2014 Szymon Kuchnia (szymon@kuchnia.name)
 *
 * Autor nie ponoszą odpowiedzialności za jakiekolwiek szkody wynikające z korzystania z danego oprogramowania.
 *
 * Udziela się zgody do korzystania z tego oprogramowania w dowolnym celu, w tym zastosowań
 * komercyjnych, a także zmieniać je i rozpowszechniać dowolnie z zastrzeżeniem następujących ograniczeń:
 *
 * 1. Pochodzenie oprogramiwania nie moze być nieprawdziwe.
 *    Państwo nie mogą twierdzić że napisali dane oprogramowanie.
 *
 * 2. Zmienione wersje źródłowe musza być oznaczone i nie mogą być porównywane
 *    z wersjami orginalnymi.
 *
 * 3. Ta informacja nie moze być zmieniona lub usunięta w każdej dystrybucji źródłowej.
 */

#ifndef __LOGGER_LOGINFO_H__
#define __LOGGER_LOGINFO_H__

namespace Logger {

    /**********************************************//**
     * @brief Struktura opisująca informacje o ostatnim logu
     ************************************************/
    struct LogInfo {
        std::string date;     /**< Data utorzenia loga */
        std::string file;     /**< Plik z którego pochodzi log */
        std::string function; /**< Funkcja która wywołała loga */
        int line;             /**< Linia z której pochodzi log */
    };

}

#endif /* __LOGGER_LOGINFO_H__ */
