/**
 * Logger
 * Copyright (C) 2014 Szymon Kuchnia (szymon@kuchnia.name)
 *
 * Autor nie ponoszą odpowiedzialności za jakiekolwiek szkody wynikające z korzystania z danego oprogramowania.
 *
 * Udziela się zgody do korzystania z tego oprogramowania w dowolnym celu, w tym zastosowań
 * komercyjnych, a także zmieniać je i rozpowszechniać dowolnie z zastrzeżeniem następujących ograniczeń:
 *
 * 1. Pochodzenie oprogramiwania nie moze być nieprawdziwe.
 *    Państwo nie mogą twierdzić że napisali dane oprogramowanie.
 *
 * 2. Zmienione wersje źródłowe musza być oznaczone i nie mogą być porównywane
 *    z wersjami orginalnymi.
 *
 * 3. Ta informacja nie moze być zmieniona lub usunięta w każdej dystrybucji źródłowej.
 */

#ifndef __LOGGER_LOGGER_H__
#define __LOGGER_LOGGER_H__

#include <Logger/Core/Export.h>
#include <Logger/Core/Manipulator.h>
#include <Logger/Core/Priority.h>
#include <Logger/Core/LogInfo.h>

#include <sstream>
#include <vector>
#include <ostream>

namespace Logger {

    class AOutput;

    class LOGGER_API StreamLogger {
    public:
        LogInfo info; /**< Informację o logu */

    protected:
        std::stringstream currentMsg;  /**< Bierząca wiadomość */
        Priority currentPriority;      /**< Bierzacy priorytet wiadomosci */

        std::vector<AOutput*> outputs; /**< Wyjścia loggera */

        bool online; /**< Logger załączony */

    public:
        StreamLogger() : currentPriority(LP_None), online(true) { info.line = -1; }
        ~StreamLogger();

    public:
        /**********************************************//**
         * @brief Doczepianie wyjścia
         *
         * @param output Adres nowego doczepianego wyjścia
         *
         * @retval true  Udało się doczepić wyjście
         * @retval false Operacja zakończyła się niepowodzeniem
         ************************************************/
        bool addOutput(AOutput *output);

        /**********************************************//**
         * @brief Odczepianie wyjścia
         *
         * Przy odpinaniu wyjście nie jest usuwane z pamięci.
         * Trzeba pamiętać żeby ręcznie zwolnić pamięć.
         *
         * @param output Adres odczepianego wyjścia
         *
         * @retval true  Udało się odczepić wyjście
         * @retval false Operacja zakończyła się niepowodzeniem
         ************************************************/
        bool removeOutput(AOutput *output);

        /**********************************************//**
         * @brief Ustawia priorytet
         ************************************************/
        void setPriority(const Priority &priority) {
            currentPriority = priority;
        }

        /**********************************************//**
         * @brief Zwraca aktualny priorytet
         ************************************************/
        Priority getPriority() {
          return currentPriority;
        }

        /**********************************************//**
         * @brief Załącza logger
         ************************************************/
        void on() {
          online = true;
        }

        /**********************************************//**
         * @brief Załącza logger
         ************************************************/
        void off() {
          online = false;
        }

        /**********************************************//**
         * @brief Zwraca informację czy logger jest włączony
         ************************************************/
        bool isOnline() const {
          return online;
        }

        /**********************************************//**
         * @brief Wysyłanie komunikatu do wyjść logger'a odnośnie zapisu.
         *
         * Buffor @b currentMsg po wywołaniu metody zostaje wyczyszczony.
         ************************************************/
        void write();

        /**********************************************//**
         * @brief Loggowanie w stylu C
         ************************************************/
        void log(const Priority &priority, const char *format, ...);

        /**********************************************//**
          Operator obsługuje manupulatory które przyjmują i zwracają @b std::ostream&

          @param funPtr Adres do funkcji która ma zostać obsłużona
         ************************************************/
//        StreamLogger& operator<< (std::ostream& (*funPtr)(std::ostream&)) {
//            funPtr(*currentMsg);
//          return *this;
//        }

        /**********************************************//**
         * const char*
         ************************************************/
        StreamLogger& operator<< (const char *value) {
          return operatorWrite(value);
        }

        /**********************************************//**
         * std::string
         ************************************************/
        StreamLogger& operator<< (const std::string &value) {
          return operatorWrite(value);
        }

        /**********************************************//**
         * char
         ************************************************/
        StreamLogger& operator<< (const char &value) {
          return operatorWrite(value);
        }

        /**********************************************//**
         * bool
         ************************************************/
        StreamLogger& operator<< (const bool &value) {
          return operatorWrite(value);
        }

        /**********************************************//**
         * unsigned short
         ************************************************/
        StreamLogger& operator<< (const unsigned short &value) {
          return operatorWrite(value);
        }

        /**********************************************//**
         * short
         ************************************************/
        StreamLogger& operator<< (const short &value) {
          return operatorWrite(value);
        }

        /**********************************************//**
         * unsigned int
         ************************************************/
        StreamLogger& operator<< (const unsigned int &value) {
          return operatorWrite(value);
        }

        /**********************************************//**
         * int
         ************************************************/
        StreamLogger& operator<< (const int &value) {
          return operatorWrite(value);
        }

        /**********************************************//**
         * float
         ************************************************/
        StreamLogger& operator<< (const float &value) {
          return operatorWrite(value);
        }

        /**********************************************//**
         * double
         ************************************************/
        StreamLogger& operator<< (const double &value) {
          return operatorWrite(value);
        }

        /**********************************************//**
         * void*
         ************************************************/
        StreamLogger& operator<< (void *value) {
          return operatorWrite(value);
        }

        /**********************************************//**
         * @brief Manipulator priorytetu wiadomości
         ************************************************/
        StreamLogger& operator<< (const Prio &prio) {
            currentPriority = prio.priority;
          return *this;
        }

        /**********************************************//**
         * @brief Manipulator każący wysłać wiadomość do wszystkich wyjść
         ************************************************/
        StreamLogger& operator<< (const Flush &flush) {
            write();
          return *this;
        }

    protected:
        /**********************************************//**
          Metoda odnosząca się do przeciążenia operatorów.
          Stworzona by zapobiec kopiowaniu kodu.
         ************************************************/
        template<typename T> StreamLogger& operatorWrite(const T &val) {
            currentMsg << val;
          return *this;
        }
    };

}


#endif /* __LOGGER_LOGGER_H__ */
