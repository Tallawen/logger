/**
 * Logger
 * Copyright (C) 2014 Szymon Kuchnia (szymon@kuchnia.name)
 *
 * Autor nie ponoszą odpowiedzialności za jakiekolwiek szkody wynikające z korzystania z danego oprogramowania.
 *
 * Udziela się zgody do korzystania z tego oprogramowania w dowolnym celu, w tym zastosowań
 * komercyjnych, a także zmieniać je i rozpowszechniać dowolnie z zastrzeżeniem następujących ograniczeń:
 *
 * 1. Pochodzenie oprogramiwania nie moze być nieprawdziwe.
 *    Państwo nie mogą twierdzić że napisali dane oprogramowanie.
 *
 * 2. Zmienione wersje źródłowe musza być oznaczone i nie mogą być porównywane
 *    z wersjami orginalnymi.
 *
 * 3. Ta informacja nie moze być zmieniona lub usunięta w każdej dystrybucji źródłowej.
 */

#ifndef __LOGGER_CONFIG_H__
#define __LOGGER_CONFIG_H__


/************************************************
 * Wersjce logger'a
 ************************************************/
#define LOGGER_VERSION_MAJOR 0
#define LOGGER_VERSION_MINOR 1


/************************************************
 * Rozpoznawanie systemu
 ************************************************/
#if defined(_WIN32) || defined(__WIN32__)
    // Windows
    #define LOGGER_SYSTEM_WINDOWS

#elif defined(linux) || defined(__linux)
    // Linux
    #define LOGGER_SYSTEM_LINUX

#else
    // Inny
    #error This operating system is not supported by LOGGER library

#endif

#if !defined(NDEBUG)
    #define  LOGGER_DEBUG
#endif

/************************************************
 * Pomocnicze makra do toworzenia import'u / eport'u
 ************************************************/
#if !defined(LOGGER_STATIC)
    #if defined(LOGGER_SYSTEM_WINDOWS)

        #define LOGGER_API_EXPORT __declspec(dllexport)
        #define LOGGER_API_IMPORT __declspec(dllimport)

        // Dla Visual C++ musimy wyłączyć C4251 warning
        #ifdef _MSC_VER
            #pragma warning(disable : 4251)
        #endif

    #else // Linux
        #if __GNUC__ >= 4

            // GCC 4 has special keywords for showing/hidding symbols,
            // the same keyword is used for both importing and exporting
            #define LOGGER_API_EXPORT __attribute__ ((__visibility__ ("default")))
            #define LOGGER_API_IMPORT __attribute__ ((__visibility__ ("default")))

        #else

            // GCC < 4 has no mechanism to explicitely hide symbols, everything's exported
            #define LOGGER_API_EXPORT
            #define LOGGER_API_IMPORT

        #endif
    #endif

#else

    // Budowanie stayczne nie potrzebuje imort'u / export'u
    #define LOGGER_API_EXPORT
    #define LOGGER_API_IMPORT

#endif

#endif /* __LOGGER_CONFIG_H__ */
